## Goal
We would like to X which will improve Y as measured by Z.

#### Jobs To Be Done
* **Situation**: *When ...*
* **Motivation**: *We want to ...*
* **Outcome**: *So we can ...*

#### Please provide more information related to this request
Make sure the information you provide is relevant for your request. If unsure, please provide all the fields. Add/remove rows as needed.

**If this is urgent, what is the business need for the urgency?**
- [ ]  Yes: `what is the business need for the urgency`
- [ ]  No

If the requestor is not the DRI, find out if the DRI is aware of the request / wants to change things.

**Does anyone in leadership have eyes on this project?**
- [ ]  Yes `@personshandle`
- [ ]  No

**Compliance / legal / accessibility regulations to be aware of?**
- [ ] Yes: `Please describe why`
- [ ] No: `Please describe why`

**What is the ultimate end goal vision vs the MVC1 iteration?**
 - `End goal for this MVC iteration is XYZ`

**How do we measure success?**
 - `Please describe how success will be measured`

**What analytics / data will we need to set-up?**
- [ ] Yes: `Please describe what analytics will need to be set up`
- [ ] No

## Page(s)
Which page(s) are involved in this request?
* `[Page Title](URL)`

## DCI
[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI: `GitLab Handle`
- [ ] Consulted: `GitLab Handle`
- [ ] Informed: `Everyone`

## In scope
What is within scope of this request?

- [ ] Deliverable 1
- [ ] Deliverable 2

## Requirements
What are the requirements for this request? Checklist below is an example of common requirements, please check all that apply and adjust as necessary:

- [ ] Copy writing
- [ ] Illustration
- [ ] Custom Graphics
- [ ] Research
- [ ] Data / Analytics
- [ ] UX Design
- [ ] Engineering

/label ~"dex-status::triage"
